# -*- coding: utf-8 -*-
from setuptools import setup
from setuptools import find_packages

setup(
        name='my_package',  # Package name
        version='0.0.1',    # Version number, Must meet https://www.python.org/dev/peps/pep-0440/ format
        author='Ran Bao',   # Your name
        author_email="rbao@enphaseenergy.com",  # Your email address
        url='https://bitbucket.org/ran_bao/my_package/src/master/',  # The link to the project
        description='An example Enphase Python package',
        packages=find_packages(where='src'),  # Use package in src/ folder
        package_dir={'': 'src'},  # Declare that source code are in src folder
        options={'bdist_wheel': {'universal': True}},  # Build wheel for both Python 2 and 3
        include_package_data=True,  # Include non-code resource files as mentioned in MANIFEST.in
        # Define following section for dependencies (package names)
        install_requires=['pytest',
                          'coverage',
                          'pytest-cov',
                          'pytest-subtests;python_version>="3"'],
)