# -*- coding: utf-8 -*-
import logging
import unittest
import sys
import my_package
from my_package.mylib import MyLib

class TestRandomStuff(unittest.TestCase):
    def setUp(self):
        self.log = logging.getLogger(self.__class__.__name__)

    def test_a(self):
        """
        This is a random test case
        """
        print("YES")

    def test_b(self):
        self.log.debug('DEBUG')
        self.log.info('INFO')
        self.log.warning('WARNING')
        self.log.error('ERROR')

    def test_c(self):
        sys.stderr.write('Stderr\r\n')

    def test_e(self):
        a = MyLib

    def test_d(self):
        self.assertIn('site-packages', my_package.__file__)


if __name__ == '__main__':
    # logging.basicConfig(level=logging.DEBUG)
    logging.disable(logging.NOTSET)
    unittest.main()
