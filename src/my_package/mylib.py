# -*- coding: utf-8 -*-
import logging


class MyLib(object):
    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)


class MyLib2(MyLib):
    def method(self):
        print("YES")