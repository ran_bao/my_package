Example Python Package
======================
See [Packaging Python Project](https://packaging.python.org/tutorials/packaging-projects/) for more details. 

Notes
-----

NO `__init__.py` in project root directory. 

There are some arguments around whether you should put source code into `src/` folder. Personally I prefer doing so as the `src/` will provide extra isolation which prevent user from importing package without installation. 

To generate Python wheel you need: 

    pip wheel --no-deps --wheel-dir=wheel .
    
To run unttest you need to install `pytest`, and run pytest at project root directory

    pytest